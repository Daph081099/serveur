// Code de test rapide *****************************************************************************
// let http = require('http');
//
// let compteur = 0;
//
// let serveur = http.createServer((requete, reponse) => {
//   reponse.end("Allo! #" + ++compteur);
// })
//
// serveur.listen(3000, () => {
//   console.log("Serveur fonctionnel sur le port", serveur.address().port);
// })
// *************************************************************************************************


// Importation des modules de Node.js
let fs = require('fs');
// let http = require('http');
let https = require('https');
let path = require('path')
let SocketIO = require('socket.io');

// Configurations
const config = {
    adresse: "dstonge.dectim.ca",
    port: 3002,
    certificat: fs.readFileSync(path.resolve() + '/securite/certificat.pem'),
    cle: fs.readFileSync(path.resolve() + '/securite/cle.pem')
}

// Instantiation d'un serveur HTTP et d'un serveur de WebSocket utlisant le serveur HTTP comme
// couche de transport
const serveur = https.createServer({key: config.cle, cert: config.certificat});
const io = SocketIO(serveur);

// Démarrage du serveur HTTP
serveur.listen(config.port, config.adresse, () => {
    console.log("Le serveur est prêt et écoute sur le port " + serveur.address().port);
});

// Liste des connexions de toutes les interfaces (appareils mobiles)
let interfaces = [];

// Liste des connexions de tous les jeux (pages web)
let jeux = [];

let joueur = null;

// Mise en place d'un écouteur pour traiter les connexions de client et les données envoyées par
// ceux-ci.
io.on("connection", socket => {

    // On vérifie si la connexion provient d'une interface ou d'un jeu
    if (socket.handshake.query.type === "interface") {
        gererNouvelleInterface(socket);

    } else if (socket.handshake.query.type === "jeu") {
        gererNouveauJeu(socket);


    } else {
        socket.disconnect();
    }

});


// Interface telephone
function gererNouvelleInterface(socket) {


    let jeuTrouve = jeux.find(j => j.handshake.query.code === socket.handshake.query.code)
    let nomJoueur = socket.handshake.query.name;


    if (!jeuTrouve) {
        socket.emit('erreur');
    } else {
        socket.emit('succes');
        jeuTrouve.emit('infoJoueur', nomJoueur);


        socket.on('jeu', () => {

            jeuTrouve.emit("partirJeu");
        });

        socket.on('finJeuTimer', ()=>{

        });

        socket.on('overlay', ()=> {
           jeuTrouve.emit('overlay');
        });
        socket.on('overlayClose', ()=> {
            jeuTrouve.emit('overlayClose');
        });

        socket.on('quitterArcade', () =>{
            jeuTrouve.emit('quitterArcade');

            socket.emit('redemarrer');
        });
        socket.on('quitterOcean', () =>{
            jeuTrouve.emit('quitterOcean');

            socket.emit('redemarrer');
        });
        socket.on('quitterVolcan', () =>{
            jeuTrouve.emit('quitterVolcan');

            socket.emit('redemarrer');
        });
        socket.on('quitterJungle', () =>{
            jeuTrouve.emit('quitterJungle');
            socket.emit('redemarrer');
        });
        socket.on('quitterEspace', () =>{
            jeuTrouve.emit('quitterEspace');
            socket.emit('redemarrer');
        });

        socket.on('son', () =>{
            jeuTrouve.emit('son');
        });


        socket.on("posElement", message => {
            jeuTrouve.emit("joueur1", message);

        });

        socket.on('lancer', message => {
            jeuTrouve.emit("lancer", message);

        });

        socket.on('instructions', () => {
            jeuTrouve.emit("afficherInstruction");
        });

        socket.on('instructions2', () => {
            jeuTrouve.emit("afficherInstruction2");

        });



        socket.on('message', e =>{
            console.log(e);
        });

        socket.on('arcade', () => {
            jeuTrouve.emit("partirArcade");

        });

        socket.on('ocean', () => {

            jeuTrouve.emit("partirOcean");
        });

        socket.on('volcan', () => {

            jeuTrouve.emit("partirVolcan");
        });

        socket.on('jungle', () => {
            jeuTrouve.emit("partirJungle");
        });

        socket.on('espace', () => {
            console.log('allo espace');
            jeuTrouve.emit("partirEspace");
        });
    }


    // Ajout d'un écouteur pour détecter la déconnexion d'une interface
    socket.on('disconnect', () => {

        // Identification de l'index de l'interface qui vient de se déconnecter
        const index = interfaces.indexOf(socket);

        console.log("Déconnexion de l'interface: ", index);


        interfaces[index] =null;

        // Envoyer une mise à jour à toutes les interfaces pour identifer leur position dans la liste
        interfaces.forEach((interface, index) => {
            if (interface === null) return;
            // interface.emit("position", {position: index})
        })
    })

}

function gererNouveauJeu(socket) {

    console.log("Connexion d'une page web de jeu");

    jeux.push(socket);

    socket.on('disconnect', () => {
        console.log("Déconnexion d'une page de jeu!");
        jeux = jeux.filter(item => item !== socket)
    });

}


// ws://67.212.82.134:3000/socket.io/?EIO=2&transport=websocket
